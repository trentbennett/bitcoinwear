package com.redpill.bitcoin;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.redpill.bitcoin.services.responses.CoinbasePriceResponse;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends Activity {

    @Bind(R.id.bitcoin_price_textView) TextView mBitcoinPriceTextView;
    private CoinbaseWatchDataModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
//
//        model = new CoinbaseWatchDataModel(this);
//        model.onCreate();
//        model.setListener(new CoinbaseWatchDataModel.DataListener() {
//            @Override
//            public void onRequestBitcoinUpdate() {
//                retrieveBitcoinPrice();
//            }
//        });
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        model.onResume();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        model.onPause();
//    }

    private void setBitcoinPriceText(String text) {
        mBitcoinPriceTextView.setText(text);
    }

//    @OnClick(R.id.get_bitcoin_price_button)
//    public void onBitcoinPriceClicked() {
//        retrieveBitcoinPrice();
//    }

    public void retrieveBitcoinPrice() {
        Call<CoinbasePriceResponse> call = model.getCoinbasePrice();

        call.enqueue(new Callback<CoinbasePriceResponse>() {
            @Override
            public void onResponse(Response<CoinbasePriceResponse> response, Retrofit retrofit) {
                String amount = response.body().getData().getAmount();
                setBitcoinPriceText(amount);
                model.sendWearBitcoinPrice(amount);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }



}
