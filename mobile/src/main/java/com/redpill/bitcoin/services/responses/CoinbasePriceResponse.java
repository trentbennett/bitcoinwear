package com.redpill.bitcoin.services.responses;

import com.redpill.bitcoin.services.models.CoinbasePriceData;

public class CoinbasePriceResponse {
    private CoinbasePriceData data;

    public CoinbasePriceData getData() {
        return data;
    }

    public void setData(CoinbasePriceData data) {
        this.data = data;
    }
}
