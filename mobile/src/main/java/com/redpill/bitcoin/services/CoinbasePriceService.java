package com.redpill.bitcoin.services;

import com.redpill.bitcoin.services.responses.CoinbasePriceResponse;

import retrofit.Call;
import retrofit.http.GET;

public interface CoinbasePriceService {

    @GET("/v2/prices/buy")
    Call<CoinbasePriceResponse> getCoinbasePrice();
}
