package com.redpill.bitcoin.services;

public interface ApiCallback<T> {
    void onSuccess(T object);
    void onFailure(String message);
}
