package com.redpill.bitcoin.services;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.redpill.bitcoin.CoinbaseWatchDataModel;
import com.redpill.bitcoin.services.responses.CoinbasePriceResponse;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class BitcoinWearListenerService extends WearableListenerService {

    private static final String TAG = "BitcoinWearListenerServ";

    private static final String BTC_PRICE_KEY = "com.redpill.bitcoin.price";


    private CoinbaseWatchDataModel model;

    @Override
    public void onCreate() {
        super.onCreate();

        model = new CoinbaseWatchDataModel(getApplicationContext());
        model.onCreate();
        model.onResume();

        retrieveBitcoinPrice();

        Log.d(TAG, "onCreate");
    }


    public void retrieveBitcoinPrice() {
        Call<CoinbasePriceResponse> call = model.getCoinbasePrice();

        call.enqueue(new Callback<CoinbasePriceResponse>() {
            @Override
            public void onResponse(Response<CoinbasePriceResponse> response, Retrofit retrofit) {
                model.sendWearBitcoinPrice(response.body().getData().getAmount());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        model.onPause();
        Log.d(TAG, "onDestroy");
    }
}
