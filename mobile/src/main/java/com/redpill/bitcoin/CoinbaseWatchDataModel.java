package com.redpill.bitcoin;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.redpill.bitcoin.services.CoinbasePriceService;
import com.redpill.bitcoin.services.responses.CoinbasePriceResponse;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class CoinbaseWatchDataModel implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DataApi.DataListener, MessageApi.MessageListener {

    private DataListener listener;

    public interface DataListener {
        void onRequestBitcoinUpdate();
    }

    private static final String BTC_PRICE_KEY = "com.redpill.bitcoin.price";

    private Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private CoinbasePriceService mCoinbaseService;


    public CoinbaseWatchDataModel(Context context) {
        mContext = context;
    }

    public void setListener(DataListener listener) {
        this.listener = listener;
    }

    public void onCreate() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.coinbase.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mCoinbaseService = retrofit.create(CoinbasePriceService.class);
    }

    public void onResume() {
        mGoogleApiClient.connect();
    }

    public void onPause() {
        Wearable.DataApi.removeListener(mGoogleApiClient, this);
        Wearable.MessageApi.removeListener(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }


    public Call<CoinbasePriceResponse> getCoinbasePrice() {
        return mCoinbaseService.getCoinbasePrice();
    }

    public void sendWearBitcoinPrice(String price) {
        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/btc_price");
        putDataMapReq.getDataMap().putString(BTC_PRICE_KEY, price);
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Wearable.DataApi.addListener(mGoogleApiClient, this);
        Wearable.MessageApi.addListener(mGoogleApiClient, this);

    }

    @Override
    public void onConnectionSuspended(int cause) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
    }


    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String path = messageEvent.getPath();
        if("/update".equals(path) && listener != null)
            listener.onRequestBitcoinUpdate();
    }
}
