package com.redpill.bitcoin;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.redpill.bitcoin.model.BitcoinPrice;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WearDataModel implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DataApi.DataListener {

    private static final String COUNT_KEY = "com.redpill.bitcoin.count";
    private static final String BTC_PRICE_KEY = "com.redpill.bitcoin.price";

    private static final String BITCOIN_SHARED_PREF = "bitcoin_shared_preferences";
    private static final String SP_BTC_PRICE = "btc_price";
    private static final String SP_LAST_UPDATE = "last_update";

    private static final long TIMEOUT = 10000;

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("M/dd/yy h:mm a");

    private final Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private Node peerNode;
    private WearDataCallback mCallback;
    private SharedPreferences sharedPreferences;
    private boolean isMakingCall;


    interface WearDataCallback {
        void onBitcoinPriceReceived(BitcoinPrice bitcoinPrice);
        void onTimeoutReached();
    }

    public WearDataModel(Context context) {
        mContext = context;
    }

    public void setCallback(WearDataCallback callback) {
        mCallback = callback;
    }

    public void onCreate() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        sharedPreferences = mContext.getSharedPreferences(BITCOIN_SHARED_PREF, Context.MODE_PRIVATE);
    }

    public void onResume() {
        mGoogleApiClient.connect();
    }

    public void onPause() {
        Wearable.DataApi.removeListener(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }


    public void requestBitcoinPrice() {
        new RetrieveNodesTask().execute();
    }

    private void sendBitcoinPriceRequest(NodeApi.GetConnectedNodesResult rawNodes) {

        for (final Node node : rawNodes.getNodes()) {
            Log.v("TAG", "Node: " + node.getId());
            PendingResult<MessageApi.SendMessageResult> result = Wearable.MessageApi.sendMessage(
                    mGoogleApiClient,
                    node.getId(),
                    "/update",
                    null
            );

            result.setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                @Override
                public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                    //  The message is done sending.
                    //  This doesn't mean it worked, though.
                    Log.v("TAG", "Our callback is done.");
                    //Todo figure out what todo with peerNode
                    peerNode = node;    //  Save the node that worked so we don't have to loop again.
                }
            });
        }

        isMakingCall = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isMakingCall) {
                    isMakingCall = false;
                    if(mCallback != null)
                        mCallback.onTimeoutReached();
                }
            }
        }, TIMEOUT);
    }

    public BitcoinPrice getLastBitcoinPrice() {
        BitcoinPrice bitcoinPrice = null;

        String amount = sharedPreferences.getString(SP_BTC_PRICE, null);
        String timeStamp = sharedPreferences.getString(SP_LAST_UPDATE, null);

        if (amount != null && timeStamp != null)
            bitcoinPrice = new BitcoinPrice(amount, timeStamp);

        return bitcoinPrice;
    }

    public void saveBitcoinPrice(BitcoinPrice bitcoinPrice) {
        sharedPreferences.edit()
                .putString(SP_BTC_PRICE, bitcoinPrice.getAmount())
                .putString(SP_LAST_UPDATE, bitcoinPrice.getTimeStamp())
                .apply();
    }


    @Override
    public void onConnected(Bundle bundle) {
        Wearable.DataApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d("onDataChanged", "Called");
        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                Log.d("onDataChanged", "Type Changed");
                // DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo("/btc_price") == 0) {
                    isMakingCall = false;
                    final DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();

                    BitcoinPrice bitcoinPrice = new BitcoinPrice();
                    bitcoinPrice.setAmount(dataMap.getString(BTC_PRICE_KEY));
                    bitcoinPrice.setTimeStamp(dateFormat.format(new Date()));

                    if (mCallback != null)
                        mCallback.onBitcoinPriceReceived(bitcoinPrice);

                    Log.d("onDataChanged", "BTC Updated");
                }
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                Log.d("onDataChanged", "Type Deleted");            }
        }
    }

    class RetrieveNodesTask extends AsyncTask<Void, Void, NodeApi.GetConnectedNodesResult> {

        @Override
        protected NodeApi.GetConnectedNodesResult doInBackground(Void... params) {
            NodeApi.GetConnectedNodesResult rawNodes =
                    Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
            return rawNodes;
        }

        @Override
        protected void onPostExecute(NodeApi.GetConnectedNodesResult rawNodes) {
            sendBitcoinPriceRequest(rawNodes);
        }
    }
}
