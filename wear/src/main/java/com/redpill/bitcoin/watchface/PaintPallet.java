package com.redpill.bitcoin.watchface;

import android.graphics.Paint;

public class PaintPallet {
    public static Paint makeBtcOrange() {
        Paint paint = makePaint(255, 255, 153, 0);
        return paint;
    }
    public static Paint makeBlack() {
        Paint paint = makePaint(255, 0, 0, 0);
        return paint;
    }

    public static Paint makeWhitePaint() {
        Paint paint = makePaint(255, 255, 255, 255);
        return paint;
    }

    public static Paint makeWhiteTextCenter(int textSize) {
        Paint paint = makeWhitePaint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(textSize);
        return paint;
    }

    public static Paint makePaint(int a, int r, int g, int b) {
        Paint paint = new Paint();
        paint.setARGB(a, r, g, b);
        paint.setAntiAlias(true);
        return paint;
    }
}
