package com.redpill.bitcoin.watchface;

import com.redpill.bitcoin.model.BitcoinPrice;

public interface MainView {
    void updateScreen(BitcoinPrice bitcoinPrice);
    void setBitcoinPriceText(String bitcoinPrice);
    void setLoadingState(int loadingState);
    void hideLastUpdated(boolean hide);
    void setLastUpdatedText(String dateTime);
}
