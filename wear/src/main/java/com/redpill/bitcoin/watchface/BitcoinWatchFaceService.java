package com.redpill.bitcoin.watchface;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.view.SurfaceHolder;

import com.redpill.bitcoin.WearDataModel;
import com.redpill.bitcoin.model.BitcoinPrice;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class BitcoinWatchFaceService extends CanvasWatchFaceService {
    @Override
    public Engine onCreateEngine() {
        /* provide your watch face implementation */
        return new Engine();

    }

    /* implement service callback methods */
    private class Engine extends CanvasWatchFaceService.Engine {

        private SimpleDateFormat mTimeFormat = new SimpleDateFormat("h:mm a", Locale.US);

        private WearDataModel model;
        private BitcoinPrice bitcoinPrice;

        private Calendar mCalendar;
        private Paint mBackgroundPaint;
        private Paint mBtcOrangePaint;
        private Paint mWhitePaint;
        private Paint mClockTextPaint;
        private Paint mPriceTextPaint;
        private boolean mLowBitAmbient;
        private boolean mBurnInProtection;
        private int width, height;

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            model = new WearDataModel(getApplicationContext());
            model.onCreate();
            model.onResume();

            Resources resources = BitcoinWatchFaceService.this.getResources();

            initPaints();

            mCalendar = Calendar.getInstance();

        }

        public void initPaints() {
            mBackgroundPaint = PaintPallet.makeBlack();
            mBtcOrangePaint = PaintPallet.makeBtcOrange();
            mWhitePaint = PaintPallet.makeWhitePaint();
            mWhitePaint.setStrokeWidth(10);
            mWhitePaint.setStyle(Paint.Style.STROKE);
            mClockTextPaint = PaintPallet.makeWhiteTextCenter(50);
            mPriceTextPaint = PaintPallet.makeWhiteTextCenter(36);
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);

            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION, false);

        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();

            bitcoinPrice = model.getLastBitcoinPrice();

            Calendar now = Calendar.getInstance();
            if(now.get(Calendar.MINUTE)%15 == 0 || bitcoinPrice == null) {
                model.requestBitcoinPrice();
            }

            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);

            if(inAmbientMode) {
                boolean antiAlias = !inAmbientMode;
                mBackgroundPaint.setAntiAlias(antiAlias);
                mPriceTextPaint.setAntiAlias(antiAlias);
            }
            invalidate();
        }

        @Override
        public void onSurfaceChanged(
                SurfaceHolder holder, int format, int width, int height) {
//            if (mBackgroundScaledBitmap == null
//                    || mBackgroundScaledBitmap.getWidth() != width
//                    || mBackgroundScaledBitmap.getHeight() != height) {
//                mBackgroundScaledBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap,
//                        width, height, true /* filter */);
//            }
            super.onSurfaceChanged(holder, format, width, height);
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {

            mCalendar.setTimeInMillis(System.currentTimeMillis());

            width = bounds.width();
            height = bounds.height();

            //Draw Black Background
            canvas.drawPaint(mBackgroundPaint);

            drawClock(canvas);
            drawCoin(canvas);
            drawBtcPrice(canvas);

        }

        private void drawClock(Canvas canvas) {

        }

        private void drawCoin(Canvas canvas) {
            float cX = (width/2f);
            float cY = (height/2f);
            float radius = (width * 0.8f) / 2;
            canvas.drawCircle(cX, cY, radius, mBtcOrangePaint);

            float ringMargin = 8;

            canvas.drawCircle(cX, cY, radius - ringMargin, mWhitePaint);
        }

        private void drawBtcPrice(Canvas canvas) {
            float clockX = (width/2f);
            float clockY = (height/2f);

            if(bitcoinPrice != null) {
                float priceX = width/2f;
                float priceY = height/2f + 50f;

                canvas.drawText("$" + bitcoinPrice.getAmount(), priceX, priceY, mPriceTextPaint);
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            /* the watch face became visible or invisible */
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            model.onPause();
        }
    }
}
