package com.redpill.bitcoin.model;

public class BitcoinPrice {
    private String amount;
    private String timeStamp;

    public BitcoinPrice() {}

    public BitcoinPrice(String amount, String timeStamp) {
        setAmount(amount);
        setTimeStamp(timeStamp);
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
