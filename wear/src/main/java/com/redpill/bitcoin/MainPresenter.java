package com.redpill.bitcoin;

import android.app.Activity;
import android.content.Context;
import android.os.Vibrator;

import com.redpill.bitcoin.model.BitcoinPrice;
import com.redpill.bitcoin.watchface.MainView;

import java.util.Calendar;

public class MainPresenter implements WearDataModel.WearDataCallback {

    private final Activity context;
    private MainView view;
    private WearDataModel model;

    public MainPresenter(Activity context, MainView view) {
        model = new WearDataModel(context);
        model.onCreate();
        model.setCallback(this);
        this.view = view;
        this.context = context;

    }

    public void onCreate() {
        BitcoinPrice lastPrice = model.getLastBitcoinPrice();
        view.updateScreen(lastPrice);
        requestBitcoinPrice();

        view.setLoadingState(LoadingState.COMPLETE);

        //For play store screenshot
//        BitcoinPrice bitcoinPrice = new BitcoinPrice("420.42", "4/15/16 10:26am");
//        view.updateScreen(bitcoinPrice);
    }

    public void onResume() {
        model.onResume();
    }

    public void onPause() {
        model.onPause();
    }

    private void requestBitcoinPrice() {
        model.requestBitcoinPrice();
    }

    @Override
    public void onBitcoinPriceReceived(final BitcoinPrice bitcoinPrice) {
        model.saveBitcoinPrice(bitcoinPrice);
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setLoadingState(LoadingState.COMPLETE);
                view.updateScreen(bitcoinPrice);
            }
        });
    }

    @Override
    public void onTimeoutReached() {
        view.setLoadingState(LoadingState.COMPLETE);
    }


    public void onUpdatePriceClicked() {
        vibrate(100);
        view.setLoadingState(LoadingState.LOADING);
        requestBitcoinPrice();
    }

    private void vibrate(long duration) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(duration);
    }
}
