package com.redpill.bitcoin;

import android.app.Activity;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.wearable.view.ProgressSpinner;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.redpill.bitcoin.model.BitcoinPrice;
import com.redpill.bitcoin.watchface.MainView;

public class MainActivity extends Activity implements MainView {

    private TextView mBitcoinTextView;
    private TextView mLastUpdatedLabel;
    private TextView mLastUpdatedText;
    private Button mUpdateButton;
    private ProgressBar mProgressSpinner;

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(this, this);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                initViews(stub);
                mUpdateButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.onUpdatePriceClicked();
                    }
                });
                presenter.onCreate();
            }
        });

    }

    private void initViews(WatchViewStub stub) {
        mUpdateButton = (Button) stub.findViewById(R.id.imageView);
        mBitcoinTextView = (TextView) stub.findViewById(R.id.text);
        mLastUpdatedLabel = (TextView) stub.findViewById(R.id.last_updated_label);
        mLastUpdatedText = (TextView) stub.findViewById(R.id.last_updated);
        mProgressSpinner = (ProgressBar) stub.findViewById(R.id.progressSpinner);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }


    @Override
    public void updateScreen(BitcoinPrice bitcoinPrice) {
        if (bitcoinPrice == null) {
            setBitcoinPriceText("---");
            hideLastUpdated(true);
        }
        else {
            setBitcoinPriceText(bitcoinPrice.getAmount());
            setLastUpdatedText(bitcoinPrice.getTimeStamp());
            hideLastUpdated(false);
        }
    }


    @Override
    public void setBitcoinPriceText(String bitcoinPrice) {
            mBitcoinTextView.setText("$"+bitcoinPrice);
    }

    @Override
    public void hideLastUpdated(boolean hide) {
        mLastUpdatedLabel.setVisibility(hide?View.INVISIBLE:View.VISIBLE);
        mLastUpdatedText.setVisibility(hide?View.INVISIBLE:View.VISIBLE);
    }

    @Override
    public void setLastUpdatedText(String dateTime) {
        mLastUpdatedText.setText(dateTime);

    }

    @Override
    public void setLoadingState(int loadingState) {
        switch (loadingState) {
            case LoadingState.COMPLETE:
                mProgressSpinner.setVisibility(View.GONE);
                mUpdateButton.setVisibility(View.VISIBLE);
                break;
            case LoadingState.LOADING:
                mProgressSpinner.setVisibility(View.VISIBLE);
                mUpdateButton.setVisibility(View.GONE);
                break;
        }
    }

}
